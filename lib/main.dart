import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learncleanarch/poems/presentation/bloc/poems_screen_bloc.dart';
import 'package:learncleanarch/poems/presentation/poems_screen.dart';
import 'package:learncleanarch/service_locator.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => PoemsScreenBloc(),
      child: MaterialApp(
        title: 'Poems',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: PoemsScreen(),
      ),
    );
  }
}