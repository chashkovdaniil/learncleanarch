import 'package:get_it/get_it.dart';
import 'package:learncleanarch/poems/domain/usecases/poems_use_case.dart';
import 'package:learncleanarch/poems/presentation/poems_screen.dart';

class ServiceLocator {
  final getIt = GetIt.instance;

  ServiceLocator();

  void setup(){
    GetIt.I.registerSingleton(PoemsUseCase());
  }
}