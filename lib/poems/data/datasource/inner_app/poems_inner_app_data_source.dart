import 'dart:convert';
import 'dart:io';

import 'package:flutter/services.dart';
import 'package:learncleanarch/poems/data/datasource/poems_data_source.dart';
import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';

class PoemsInnerAppDataSource extends PoemsDataSource {
  @override
  Future<Iterable<PoemEntity>> getAll() async {

    var jsonString = await rootBundle.loadString('assets/poems.json');//await File('assets/poems.json').readAsString();
    print((File('assets/poems.json').path));
    Iterable<dynamic> jsonPoems = jsonDecode(jsonString);
    var poems = jsonPoems.map((e) => PoemEntity.fromJSON(e as Map<String, dynamic>));
    return poems;
  }
}
