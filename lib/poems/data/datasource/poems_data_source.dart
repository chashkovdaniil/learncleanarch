import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';

abstract class PoemsDataSource{
  Future<Iterable<PoemEntity>> getAll();
}