import 'package:learncleanarch/poems/data/datasource/poems_data_source.dart';
import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';

class PoemsLocalDataSource extends PoemsDataSource {
  @override
  Future<Iterable<PoemEntity>> getAll() async {
    return Future.delayed(
        Duration.zero,
        () => [
              PoemEntity(
                uid: "asd-asda-q21",
                title: "Title 1",
                poem: "Poem 1",
              ),
            ]);
    // return [
    //   PoemEntity(
    //     uid: "asd-asda-q21",
    //     title: "Title 1",
    //     poem: "Poem 1",
    //   ),
    // ];
  }
}
