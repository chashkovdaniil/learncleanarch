import 'package:learncleanarch/poems/data/datasource/poems_data_source.dart';
import 'package:learncleanarch/poems/data/repositories/poems_repository.dart';
import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';

class PoemsRepositoryImpl implements PoemsRepository {
  final PoemsDataSource dataSourceInnerApp;
  final PoemsDataSource dataSourceLocal;

  PoemsRepositoryImpl({
    required this.dataSourceInnerApp,
    required this.dataSourceLocal,
  });

  @override
  Future<Iterable<PoemEntity>> getAll() async {
    return [
      ...await dataSourceInnerApp.getAll(),
      ...await dataSourceLocal.getAll()
    ];
  }
}
