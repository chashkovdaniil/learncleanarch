import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';

abstract class PoemsRepository {
  Future<Iterable<PoemEntity>> getAll();
}