import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:learncleanarch/poems/presentation/bloc/poems_screen_bloc.dart';

class PoemsScreen extends StatelessWidget {
  const PoemsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    context.read<PoemsScreenBloc>().add(PoemsScreenInitialEvent());
    return Scaffold(
      appBar: AppBar(
        title: const Text('Poems'),
      ),
      body: BlocBuilder<PoemsScreenBloc, PoemsScreenState>(
        builder: (bloc, state) {
          if (state is PoemsScreenInitial) {
            return Center(child: CircularProgressIndicator());
          } else if (state is PoemsScreenLoadFailed) {
            return Center(child: Text('Произошла ошибка при загрузке данных'));
          }

          return ListView(
            children: (state as PoemsScreenLoadSuccess)
                .poems
                .map((e) => ListTile(
                      title: Text(e.title),
                      subtitle: Text(e.poem),
                    ))
                .toList(),
          );
        },
      ),
    );
  }
}
