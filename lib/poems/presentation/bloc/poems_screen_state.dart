part of 'poems_screen_bloc.dart';

abstract class PoemsScreenState {
  const PoemsScreenState();
}

class PoemsScreenInitial extends PoemsScreenState {
  PoemsScreenInitial();
}

class PoemsScreenLoadSuccess extends PoemsScreenState {
  Iterable<PoemEntity> poems;

  PoemsScreenLoadSuccess({this.poems = const []});
}

class PoemsScreenLoadFailed extends PoemsScreenState {}
