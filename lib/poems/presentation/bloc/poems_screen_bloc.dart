import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';
import 'package:learncleanarch/poems/domain/usecases/poems_use_case.dart';
import 'package:learncleanarch/service_locator.dart';

part 'poems_screen_event.dart';

part 'poems_screen_state.dart';

class PoemsScreenBloc extends Bloc<PoemsScreenEvent, PoemsScreenState> {
  ServiceLocator _locator = ServiceLocator();

  PoemsScreenBloc() : super(PoemsScreenInitial()) {
    _locator.setup();
  }

  @override
  Stream<PoemsScreenState> mapEventToState(
    PoemsScreenEvent event,
  ) async* {
    if (event is PoemsScreenInitialEvent) {
      yield* mapInitialEventToState();
    }
  }

  Stream<PoemsScreenState> mapInitialEventToState() async* {
    try {
      var poems = await GetIt.I<PoemsUseCase>().getAll();
      yield PoemsScreenLoadSuccess(poems: poems);
    } catch (e) {
      yield PoemsScreenLoadFailed();
    }

  }
}
