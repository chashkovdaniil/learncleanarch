class PoemEntityWithoutUID {
  final String title;
  final String poem;

  PoemEntityWithoutUID({
    required this.title,
    required this.poem,
  });
}
