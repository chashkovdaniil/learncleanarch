class PoemEntity {
  final String uid;
  final String title;
  final String poem;

  PoemEntity({
    required this.uid,
    required this.title,
    required this.poem,
  });

  factory PoemEntity.fromJSON(Map<String, dynamic> json) {
    return PoemEntity(
      uid: json['uid'] as String,
      title: json['title'] as String,
      poem: json['poem'] as String,
    );
  }
}
