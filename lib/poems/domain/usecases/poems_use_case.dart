import 'package:learncleanarch/poems/data/datasource/inner_app/poems_inner_app_data_source.dart';
import 'package:learncleanarch/poems/data/datasource/local/poems_local_data_source.dart';
import 'package:learncleanarch/poems/data/repositories/poems_repository.dart';
import 'package:learncleanarch/poems/data/repositories/poems_repository_impl.dart';
import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';

class PoemsUseCase {
  final PoemsRepository _poemsRepository = PoemsRepositoryImpl(
    dataSourceInnerApp: PoemsInnerAppDataSource(),
    dataSourceLocal: PoemsLocalDataSource(),
  );

  PoemsUseCase();

  Future<Iterable<PoemEntity>> getAll() async {
    return await _poemsRepository.getAll();
  }
}
