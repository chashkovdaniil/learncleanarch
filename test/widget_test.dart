// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:learncleanarch/poems/domain/entities/poem_entity.dart';
import 'package:test/test.dart';

import 'package:learncleanarch/poems/domain/entities/poem_entity_withoud_uid.dart';
import 'package:learncleanarch/poems/domain/usecases/poems_use_case.dart';

void main() async {
  Iterable<PoemEntity> poems = await PoemsUseCase().getAll();
  test(
    'Get length poems is not empty',
    () {
      expect(poems.isNotEmpty, equals(true));
    },
  );
  test('All poems have uid and uid is not empty', () {
    expect(poems.where((p) => p.uid.isEmpty).length, equals(0));
  });
  test('All poems have title and title is not empty', () {
    expect(poems.where((p) => p.title.isEmpty).length, equals(0));
  });
  test('All poems have poem and poem is not empty', () {
    expect(poems.where((p) => p.poem.isEmpty).length, equals(0));
  });
  print('--------- POEMS ---------');
  poems.forEach((element) {
    print(element.title);
    print(element.poem);
  });
  // testWidgets('Counter increments smoke test', (WidgetTester tester) async {
  //   // Build our app and trigger a frame.
  //   await tester.pumpWidget(MyApp());
  //
  //   // Verify that our counter starts at 0.
  //   expect(find.text('0'), findsOneWidget);
  //   expect(find.text('1'), findsNothing);
  //
  //   // Tap the '+' icon and trigger a frame.
  //   await tester.tap(find.byIcon(Icons.add));
  //   await tester.pump();

  // Verify that our counter has incremented.
  //   expect(find.text('0'), findsNothing);
  //   expect(find.text('1'), findsOneWidget);
  // });
}
